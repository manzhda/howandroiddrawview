package com.mda.howandroiddrawview.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by mda on 7/7/13.
 */
public class TutorialLinearLayout extends LinearLayout {
    private static final String TAG = "TutorialLinearLayout";

    public TutorialLinearLayout(final Context context) {
        super(context);
    }

    public TutorialLinearLayout(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public TutorialLinearLayout(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        Log.e(TAG, "onMeasure");
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(final boolean changed, final int l, final int t, final int r, final int b) {
        Log.e(TAG, "onLayout");
        super.onLayout(changed, l, t, r, b);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        Log.e(TAG, "onDraw");
        super.onDraw(canvas);
    }

    @Override
    protected boolean checkLayoutParams(final ViewGroup.LayoutParams p) {
        Log.e(TAG, "checkLayoutParams");
        return super.checkLayoutParams(p);
    }

    @Override
    protected LayoutParams generateLayoutParams(final ViewGroup.LayoutParams p) {
        Log.e(TAG, "generateLayoutParams");
        return super.generateLayoutParams(p);
    }

    @Override
    public LayoutParams generateLayoutParams(final AttributeSet attrs) {
        Log.e(TAG, "generateLayoutParams");
        return super.generateLayoutParams(attrs);
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        Log.e(TAG, "generateDefaultLayoutParams");
        return super.generateDefaultLayoutParams();
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        Log.e(TAG, "onTouchEvent");
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                Log.e(TAG, "onTouchEvent" + "ACTION_DOWN");
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                Log.e(TAG, "onTouchEvent" + "ACTION_POINTER_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.e(TAG, "onTouchEvent" + "ACTION_MOVE");
                break;
            case MotionEvent.ACTION_POINTER_UP:
                Log.e(TAG, "onTouchEvent" + "ACTION_POINTER_UP");
                break;
            case MotionEvent.ACTION_UP:
                Log.e(TAG, "onTouchEvent" + "ACTION_UP");
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.e(TAG, "onTouchEvent" + "ACTION_CANCEL");
                break;
        }

        return super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(final MotionEvent ev) {
        Log.e(TAG, "onInterceptTouchEvent");
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public void requestDisallowInterceptTouchEvent(final boolean disallowIntercept) {
        Log.e(TAG, "requestDisallowInterceptTouchEvent" + ", disallow = " + disallowIntercept);
        super.requestDisallowInterceptTouchEvent(disallowIntercept);
    }
}
